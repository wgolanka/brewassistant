package com.brewassistant.data

import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate


class JdbcExtentRepository(private val jdbcTemplate: NamedParameterJdbcTemplate, private val extractor: ExtentExtractor) : ExtentRepository {

    override fun <T> upsert(extentName: String, extent: MutableSet<T>): Int {
        val params: MutableMap<String, Any> = mutableMapOf()
        params["name"] = extentName
        params["extent"] = ""
        return jdbcTemplate.update("INSERT INTO all_extents (class_name, extent) VALUES (:name, :extent)" +
                "ON CONFLICT (class_name) DO UPDATE " +
                "SET extent = EXCLUDED.extent", params)
    }

    override fun findOne(extentName: String): ByteArray? {
        return jdbcTemplate.query("SELECT class_name, extent FROM all_extents WHERE class_name = \'$extentName\'",
                extractor)
    }
}