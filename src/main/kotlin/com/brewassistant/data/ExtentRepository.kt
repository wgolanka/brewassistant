package com.brewassistant.data

interface ExtentRepository {

    fun <T> upsert(extentName: String, extent: MutableSet<T>): Int

    fun findOne(extentName: String): ByteArray?
}