package com.brewassistant.data

import org.springframework.jdbc.core.ResultSetExtractor
import java.sql.ResultSet

class ExtentExtractor : ResultSetExtractor<ByteArray> {

    override fun extractData(rs: ResultSet): ByteArray? {
        if (rs.next()) {
            return rs.getBytes("extent")
        }
        return ByteArray(1)
    }
}