package com.brewassistant.data

import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate
import org.springframework.jdbc.datasource.DriverManagerDataSource
import javax.sql.DataSource


@Configuration
open class ExtentRepositoryConfig {

    @Bean
    open fun extentRepository(namedParameterJdbcTemplate: NamedParameterJdbcTemplate, extentExtractor: ExtentExtractor): ExtentRepository {
        return JdbcExtentRepository(namedParameterJdbcTemplate, extentExtractor)
    }

    @Bean
    open fun extentExtractor(): ExtentExtractor {
        return ExtentExtractor()
    }

    @Bean
    open fun namedParameterJdbcTemplate(): NamedParameterJdbcTemplate {
        return NamedParameterJdbcTemplate(dataSource())
    }


    @Bean
    open fun dataSource(): DataSource {
        val dataSource = DriverManagerDataSource()
        dataSource.setDriverClassName("org.postgresql.Driver")
        dataSource.url = "jdbc:postgresql://localhost:5432/brew_assistant"
        dataSource.username = "postgres"
        dataSource.password = "postgres"

        return dataSource
    }
}