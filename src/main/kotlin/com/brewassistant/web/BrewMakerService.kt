package com.brewassistant.web

import com.brewassistant.domain.brew.Tea
import com.brewassistant.domain.person.Person
import org.springframework.stereotype.Service

@Service
class BrewMakerService {

    fun make(person: Person, name: String, originCountry: String, caffeineContentInMg: Double, description: String, imageLink: String): Tea {
        return(Tea(name, description, imageLink, person, originCountry, caffeineContentInMg))
    }
}