package com.brewassistant.web

import com.brewassistant.domain.brew.BrewRepository
import com.brewassistant.domain.brew.Tea
import org.springframework.http.ResponseEntity
import org.springframework.http.ResponseEntity.ok
import org.springframework.stereotype.Controller
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*
import javax.servlet.http.HttpServletResponse
import javax.validation.constraints.NotBlank
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@CrossOrigin(origins = ["http://localhost:3000", "http://localhost:3000/#"], maxAge = 3600)
@Controller
@Validated
class BrewController(private val userService: UserService, private val brewMakerService: BrewMakerService,
                     private val brewRepository: BrewRepository) {

    /**
     *
     * @param name String
     * @param originCountry String
     * @param caffeineContent String
     * @param description String
     * @param imageLink String
     * @param response HttpServletResponse
     */
    @PostMapping(value = ["/add/tea"])
    fun addBrew(@RequestParam(required = true) @NotBlank name: String,
                @RequestParam(required = true) @NotBlank originCountry: String,
                @RequestParam(required = true) @NotNull caffeineContent: String,
                @RequestParam(required = true) @NotBlank @Size(max = 100) description: String,
                @RequestParam(required = true) @NotBlank imageLink: String,
                response: HttpServletResponse) {

        val person = userService.getCurrentUser()
        val tea = brewMakerService.make(person, name, originCountry, caffeineContent.toDouble(), description, imageLink)
        brewRepository.saveAndFlush(tea)
    }

    /**
     *
     * @param id String
     * @param rate Int
     * @param response HttpServletResponse
     */
    @PutMapping(value = ["/rate/brew"])
    fun rateBrew(@RequestParam(required = true) id: String, @RequestParam(required = true) rate: Int, //TODO rate range
                 response: HttpServletResponse) {

        userService.getCurrentUser().addPointsForAddingActivity()
    }

    /**
     *
     * @return ResponseEntity<Set<Tea>>
     */
    @GetMapping("/tea/extent") //TODO
    fun getTeaExtent(): ResponseEntity<List<Tea>> {
        val all = brewRepository.getAllByIdIsNotNull()
        return ok(all)
    }
}

