package com.brewassistant.domain.person

import com.brewassistant.domain.brew.Tea
import com.brewassistant.domain.dto.Discount
import com.brewassistant.domain.rating.PersonBrewRating
import com.fasterxml.jackson.annotation.JsonBackReference
import java.time.LocalDate
import javax.persistence.*

@Entity
class Brewer(override var name: String,
             override var surname: String,
             override var avatar: ByteArray?,
             override val accountCreated: LocalDate,
             override val emailAddress: String,
             override var phoneNumber: String?,
             @ElementCollection(fetch = FetchType.EAGER)
             override val brewRatings: MutableSet<PersonBrewRating>) : Person() {

    @JsonBackReference
    @OneToMany(fetch = FetchType.EAGER, cascade = [CascadeType.ALL], orphanRemoval = true, mappedBy = "author")
    override val createdBrews: MutableSet<Tea> = mutableSetOf()

    @ManyToMany(fetch = FetchType.EAGER)
    private var discounts = mutableSetOf<Discount>()
    override var points: Long = 0
        set(value) {
            if (value > field) {
                field = value
            }
        }

    constructor(person: Person, givenDiscounts: MutableSet<Discount>) :
            this(person.name,
                    person.surname,
                    person.avatar,
                    person.accountCreated,
                    person.emailAddress,
                    person.phoneNumber,
                    person.brewRatings) {

        points = person.points
        discounts = givenDiscounts
    }

    @Override
    override fun addRating(personBrewRating: PersonBrewRating) {
        if (personBrewRating.getPerson() != this) {
            throw IllegalStateException("This rating belongs to different person!")
        }
        brewRatings.add(personBrewRating)
    }

    @Override
    override fun removeRating(personBrewRating: PersonBrewRating) {
        brewRatings.remove(personBrewRating)
    }

    @Override
    override fun addPointsForAddingActivity() {
        points += 50
    }
}
