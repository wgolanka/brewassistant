package com.brewassistant.domain.person

import com.brewassistant.domain.brew.Brew
import com.brewassistant.domain.brew.Tea
import com.brewassistant.domain.dto.Base
import com.brewassistant.domain.rating.PersonBrewRating
import java.io.Serializable
import java.time.LocalDate
import javax.persistence.Entity
import javax.persistence.Inheritance
import javax.persistence.InheritanceType

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
abstract class Person : Base(), Serializable {
    abstract val name: String
    abstract val surname: String
    abstract var avatar: ByteArray?
    abstract val accountCreated: LocalDate
    abstract val points: Long
    abstract val brewRatings: MutableSet<PersonBrewRating>
    abstract val createdBrews: MutableSet<Tea>


    abstract fun addPointsForAddingActivity()
    abstract fun addRating(personBrewRating: PersonBrewRating)
    abstract fun removeRating(personBrewRating: PersonBrewRating)

    fun addCreatedBrew(brew: Tea) {
        if (!createdBrews.contains(brew)) {
            createdBrews.add(brew)

            brew.setBrewAuthor(this)
        }
    }

    fun removeBrew(brew: Brew) {
        createdBrews.remove(brew)
    }
}