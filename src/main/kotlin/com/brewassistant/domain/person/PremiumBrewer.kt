package com.brewassistant.domain.person

import com.brewassistant.domain.brew.Tea
import com.brewassistant.domain.dto.Discount
import com.brewassistant.domain.rating.PersonBrewRating
import com.fasterxml.jackson.annotation.JsonBackReference
import java.time.LocalDate
import java.util.*
import javax.persistence.Entity
import javax.persistence.ManyToMany
import javax.persistence.OneToMany

@Entity
class PremiumBrewer(val premiumSince: LocalDate,
                    override var name: String,
                    override var surname: String,
                    override var avatar: ByteArray?,
                    override val accountCreated: LocalDate,
                    override val emailAddress: String,
                    override var phoneNumber: String?)
    : PremiumPerson() {

    @JsonBackReference
    @OneToMany
    override val createdBrews: MutableSet<Tea> = mutableSetOf()

    @OneToMany
    override val brewRatings = mutableSetOf<PersonBrewRating>()

    @ManyToMany
    override var discounts = mutableSetOf<Discount>()
    override var points: Long = 0
        set(value) {
            if (value > field) {
                field = value
            }
        }

    constructor(person: Person, givenDiscounts: MutableSet<Discount>) :
            this(LocalDate.now(),
                    person.name,
                    person.surname,
                    person.avatar,
                    person.accountCreated,
                    person.emailAddress,
                    person.phoneNumber) {

        points = person.points
        discounts = givenDiscounts
    }

    override fun addRating(personBrewRating: PersonBrewRating) {
        if (personBrewRating.getPerson() != this) {
            throw IllegalStateException("This rating belongs to different person!")
        }
        brewRatings.add(personBrewRating)
    }

    override fun removeRating(personBrewRating: PersonBrewRating) {
        brewRatings.remove(personBrewRating)
    }

    override fun addPointsForAddingActivity() {
        points.plus(1) // TODO FIX
    }

    override fun toString(): String {
        return "PremiumBrewer(premiumSince=$premiumSince, name='$name', surname='$surname', " +
                "avatar=${Arrays.toString(avatar)}, accountCreated=$accountCreated, emailAddress='$emailAddress', " +
                "phoneNumber=$phoneNumber, createdBrews=$createdBrews, brewRatings=$brewRatings, discounts=$discounts, " +
                "points=$points)"
    }
}
