package com.brewassistant.domain.person

import com.brewassistant.domain.dto.Discount
import java.io.Serializable
import javax.persistence.Entity
import javax.persistence.Inheritance
import javax.persistence.InheritanceType

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
abstract class PremiumPerson : Serializable, Person() {

    abstract var discounts: MutableSet<Discount>

    fun addDiscount(discount: Discount) {
        if (!discounts.contains(discount)) {
            discounts.add(discount)

            discount.addPerson(this)
        }
    }

    fun removeDiscount(discount: Discount) {
        discounts.remove(discount)
    }
}