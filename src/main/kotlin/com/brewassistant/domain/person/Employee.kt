package com.brewassistant.domain.person

import com.brewassistant.domain.brew.Tea
import com.brewassistant.domain.dto.Discount
import com.brewassistant.domain.rating.PersonBrewRating
import com.fasterxml.jackson.annotation.JsonBackReference
import java.io.Serializable
import java.time.LocalDate
import java.time.Period
import java.util.*
import javax.persistence.Entity
import javax.persistence.ManyToMany
import javax.persistence.OneToMany

@Entity
class Employee(private val hireDate: LocalDate,
               private var salary: Double,
               private var birthday: LocalDate,
               private var jobTitle: JobTitle,
               override val emailAddress: String,
               override var name: String,
               override var surname: String,
               override var avatar: ByteArray?,
               override val accountCreated: LocalDate,
               override var phoneNumber: String?,
               override var points: Long) : PremiumPerson(), Serializable {

    @JsonBackReference
    @OneToMany
    override val createdBrews: MutableSet<Tea> = mutableSetOf()

    @OneToMany
    override var brewRatings = mutableSetOf<PersonBrewRating>()

    @ManyToMany
    override var discounts = mutableSetOf<Discount>()

    fun calculateAge(): Int {
        return Period.between(birthday, LocalDate.now()).years
    }

    companion object {
        private const val percentRaise = 5.0
        private const val addingActivityPoints = 20
        var extent: MutableSet<Employee> = hashSetOf()
    }

    constructor(person: Person,
                hireDate:
                LocalDate,
                salary: Double,
                birthday: LocalDate,
                jobTitle: JobTitle) :
            this(hireDate,
                    salary,
                    birthday,
                    jobTitle,
                    person.emailAddress,
                    person.name,
                    person.surname,
                    person.avatar,
                    person.accountCreated,
                    person.phoneNumber,
                    person.points) {

        points = person.points
        brewRatings = person.brewRatings
    }

    override fun addPointsForAddingActivity() {
        points.plus(addingActivityPoints)
    }

    override fun addRating(personBrewRating: PersonBrewRating) {
        if (personBrewRating.getPerson() != this) {
            throw IllegalStateException("This rating belongs to different Person!")
        }
        brewRatings.add(personBrewRating)
    }

    override fun removeRating(personBrewRating: PersonBrewRating) {
        brewRatings.remove(personBrewRating)
    }

    fun raiseSalary() {
        salary += (percentRaise * salary) / 100
    }

    fun raiseSalary(percents: Double) {
        salary += (percents * salary) / 100
    }

    override fun toString(): String {
        return "Employee(hireDate=$hireDate, salary=$salary, birthday=$birthday, jobTitle=$jobTitle, " +
                "emailAddress='$emailAddress', name='$name', surname='$surname', avatar=${Arrays.toString(avatar)}, " +
                "accountCreated=$accountCreated, phoneNumber=$phoneNumber, points=$points, " +
                "createdBrews=${createdBrews.joinToString(", ")}," + " brewRatings=$brewRatings, " +
                "discounts=$discounts)"
    }
}

enum class JobTitle {
    CUSTOMER_SERVICE, TEAM_LEADER, GROUP_LEADER, CEO
}
