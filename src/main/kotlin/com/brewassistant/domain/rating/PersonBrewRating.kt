package com.brewassistant.domain.rating

import com.brewassistant.domain.brew.Brew
import com.brewassistant.domain.person.Person
import com.brewassistant.orm.AbstractJpaPersistable
import java.io.Serializable
import java.time.LocalDate
import javax.persistence.Entity

@Entity
class PersonBrewRating(private val rate: Int, private val date: LocalDate, personRating: Person, brewRating: Brew)
    : Serializable, AbstractJpaPersistable<PersonBrewRating>() {

    private var person: Person? = null
    private var brew: Brew? = null

    private fun setPerson(personToSet: Person) {
        person = personToSet
        personToSet.addRating(this)
    }

    private fun setBrew(brewToSet: Brew) {
        brew = brewToSet
        brewToSet.addRating(this)
    }

    override fun toString(): String {
        return "PersonTeaRating(rate=$rate, date=$date, person=${person?.name}, brew=${brew?.name})"
    }

    fun getPerson(): Person? {
        return person
    }

    fun getBrew(): Brew? {
        return brew
    }

    fun deleteRating() {
        person?.removeRating(this)
        brew?.removeRating(this)
        person = null
        brew = null
    }
}
