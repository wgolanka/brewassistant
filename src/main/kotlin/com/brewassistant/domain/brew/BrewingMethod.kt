package com.brewassistant.domain.brew

import com.brewassistant.domain.person.Person
import com.brewassistant.orm.AbstractJpaPersistable
import java.io.Serializable
import javax.persistence.ElementCollection
import javax.persistence.Entity

@Entity
class BrewingMethod private constructor(
        private var name: String,
        private val author: Person,
        private val brew: Brew,
        @ElementCollection
        private var ingredients: MutableSet<String>,
        private var temperature: Double?,
        private var instruction: String,
        private var time: String) : Serializable, AbstractJpaPersistable<BrewingMethod>() {

    companion object {
        var extent: MutableSet<BrewingMethod> = hashSetOf()

        fun create(
                brewMethodName: String,
                author: Person,
                brew: Brew,
                ingredients: MutableSet<String>,
                temperature: Double?,
                instruction: String,
                time: String): BrewingMethod {

            val brewMethod = BrewingMethod(brewMethodName, author, brew, ingredients, temperature, instruction, time)
            brew.addBrewMethod(brewMethod)
            extent.add(brewMethod)
            return brewMethod
        }
    }

    override fun toString(): String {
        return "BrewingMethod(name='$name', author=${author.name}, brew=${brew.name}, ingredients=$ingredients, " +
                "temperature=$temperature, instruction='$instruction', time='$time')"
    }
}
