package com.brewassistant.domain.brew

import com.brewassistant.domain.person.Person
import com.brewassistant.domain.rating.PersonBrewRating
import com.brewassistant.orm.AbstractJpaPersistable
import java.io.Serializable
import javax.persistence.Entity
import javax.persistence.Inheritance
import javax.persistence.InheritanceType

@Entity
@Inheritance(strategy = InheritanceType.JOINED)
abstract class Brew : Serializable, BrewMethodsHolder, AbstractJpaPersistable<Brew>() {
    abstract var name: String
    abstract var description: String
    abstract var imageLink: String?
    abstract var author: Person
    abstract val originCountry: String
    abstract val caffeineContent: Double
    abstract val ratings: MutableSet<PersonBrewRating>
    abstract val accessories: MutableSet<Accessory>

    abstract fun addRating(personBrewRating: PersonBrewRating)
    abstract fun removeRating(personBrewRating: PersonBrewRating)

    abstract fun addAccessory(accessory: Accessory)
    abstract fun removeAccessory(accessory: Accessory)

    fun setBrewAuthor(person: Person) {
        if (author == person) {
            return
        }
        //author is never nullable
        author.removeBrew(this)

        author = person
        author.addCreatedBrew(this as Tea)
    }
}