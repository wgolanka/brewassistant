package com.brewassistant.domain.brew

import com.brewassistant.domain.dto.Company
import com.brewassistant.orm.AbstractJpaPersistable
import java.io.Serializable
import javax.persistence.Entity
import javax.persistence.ManyToMany

@Entity
class Accessory(private var name: String,
                private var price: Double,
                private var description: String,
                private var image: ByteArray?) : Serializable, AbstractJpaPersistable<Accessory>() {

    private var company: Company? = null
    @ManyToMany
    private var brews: MutableSet<Brew> = mutableSetOf()

    fun setCompany(newCompany: Company) {
        if (company == newCompany) {
            return
        }
        if (company != null) {
            company!!.removeAccessory(this)
        }
        company = newCompany
        newCompany.addAccessory(this)
    }

    fun getCompany(): Company? {
        return company
    }

    fun getName(): String {
        return name
    }

    fun addBrew(brew: Brew) {
        if (!brews.contains(brew)) {
            brews.add(brew)
            brew.addAccessory(this)
        }
    }

    fun removeBrew(brew: Brew) {
        if (brews.contains(brew)) {
            brews.remove(brew)
            brew.removeAccessory(this)
        }
    }

    override fun toString(): String {
        return "Accessory(name='$name', price=$price, description='$description', image=${image?.size}, " +
                "company=$company," + " brews=$brews)"
    }
}