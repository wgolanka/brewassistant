package com.brewassistant.domain.brew

interface BrewMethodsHolder {

    var brewMethods: MutableSet<BrewingMethod>

    companion object {
        private var allBrewMethods: MutableSet<BrewingMethod> = mutableSetOf()
    }

    fun addBrewMethod(brewMethod: BrewingMethod) {
        if (!brewMethods.contains(brewMethod)) {
            if (allBrewMethods.contains(brewMethod)) {
                throw Exception("This Brew method is already connected with some brew")
            }

            brewMethods.add(brewMethod)
            allBrewMethods.add(brewMethod)
        }
    }
}