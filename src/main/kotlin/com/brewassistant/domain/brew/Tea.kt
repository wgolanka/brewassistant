package com.brewassistant.domain.brew

import com.brewassistant.domain.person.Person
import com.brewassistant.domain.rating.PersonBrewRating
import java.io.Serializable
import javax.persistence.*

@Entity
class Tea(override var name: String,
          override var description: String,
          override var imageLink: String?,

          @ManyToOne
          @JoinColumn(name = "person_id", nullable = false)
          override var author: Person,

          override val originCountry: String,
          override val caffeineContent: Double) : Brew(), Serializable {

    @ManyToMany(fetch = FetchType.EAGER)
    override val accessories = mutableSetOf<Accessory>()
    @OneToMany(fetch = FetchType.EAGER)
    override val ratings = mutableSetOf<PersonBrewRating>()
    @OneToMany(fetch = FetchType.EAGER)
    override var brewMethods = mutableSetOf<BrewingMethod>()

    private val maxAccessories = 5

    init {
        author.addCreatedBrew(this)
    }

    override fun addRating(personBrewRating: PersonBrewRating) {
        if (personBrewRating.getBrew() != this) {
            throw IllegalStateException("This rating belongs to different brew!")
        }
        ratings.add(personBrewRating)
    }

    override fun removeRating(personBrewRating: PersonBrewRating) {
        ratings.remove(personBrewRating)
    }

    override fun addAccessory(accessory: Accessory) {
        if (!accessories.contains(accessory) && accessories.size < maxAccessories) {
            accessories.add(accessory)

            accessory.addBrew(this)
        }
    }

    override fun removeAccessory(accessory: Accessory) {
        if (accessories.contains(accessory)) {
            accessories.remove(accessory)
            accessory.removeBrew(this)
        }
    }

    override fun toString(): String {
        return "Tea(name='$name', description='$description', imageLink=$imageLink, author=${author.name},\n" +
                " originCountry='$originCountry', caffeineContent=$caffeineContent, accessories=$accessories, \n" +
                "ratings=$ratings, brewMethods=$brewMethods, MAX_ACCESSORIES=$maxAccessories)"
    }
}