package com.brewassistant.domain.brew

import com.brewassistant.domain.person.Person
import com.brewassistant.domain.rating.PersonBrewRating
import java.io.Serializable
import javax.persistence.*

@Entity
class Coffee(override var name: String,
             override var description: String,
             override var imageLink: String?,

             @ManyToOne
             @JoinColumn(name = "person_id", nullable = false)
             override var author: Person,
             override val originCountry: String,
             override val caffeineContent: Double,
             var withMilk: Boolean) : Brew(), Serializable {

    @ManyToMany(fetch = FetchType.EAGER)
    override val accessories = mutableSetOf<Accessory>()
    @OneToMany(fetch = FetchType.EAGER)
    override val ratings = mutableSetOf<PersonBrewRating>()
    @OneToMany(fetch = FetchType.EAGER)
    override var brewMethods = mutableSetOf<BrewingMethod>()

    private val MAX_ACCESSORIES = 5

    override fun addRating(personBrewRating: PersonBrewRating) {
        if (personBrewRating.getBrew() != this) {
            throw IllegalStateException("This rating belongs to different brew!")
        }
        ratings.add(personBrewRating)
    }

    override fun removeRating(personBrewRating: PersonBrewRating) {
        ratings.remove(personBrewRating)
    }

    override fun addAccessory(accessory: Accessory) {
        if (!accessories.contains(accessory) && accessories.size < MAX_ACCESSORIES) {
            accessories.add(accessory)

            accessory.addBrew(this)
        }
    }

    override fun removeAccessory(accessory: Accessory) {
        if (accessories.contains(accessory)) {
            accessories.remove(accessory)
            accessory.removeBrew(this)
        }
    }

    override fun toString(): String {
        return "Coffee(name='$name', description='$description', imageLink=$imageLink," +
                " author=${author.name}, originCountry='$originCountry', caffeineContent=$caffeineContent, " +
                "withMilk=$withMilk, accessories=$accessories, ratings=$ratings, brewMethods=$brewMethods," +
                " MAX_ACCESSORIES=$MAX_ACCESSORIES)"
    }
}