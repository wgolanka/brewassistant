package com.brewassistant.domain.dto

import com.brewassistant.orm.AbstractJpaPersistable
import java.io.Serializable

abstract class Base : Serializable, AbstractJpaPersistable<Base>() {

    abstract val emailAddress: String
    abstract var phoneNumber: String?
}