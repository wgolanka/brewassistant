package com.brewassistant.domain.dto

import com.brewassistant.domain.brew.Accessory
import java.io.Serializable
import javax.persistence.Entity
import javax.persistence.ManyToMany
import javax.persistence.OneToMany

@Entity
class Company(private var name: String,
              private val regon: String,
              override val emailAddress: String,
              override var phoneNumber: String?) : Base(), Serializable {

    @OneToMany
    private var accessories: MutableSet<Accessory> = mutableSetOf()

    @ManyToMany
    private var discounts: MutableSet<Discount> = mutableSetOf()

    fun addAccessory(accessory: Accessory) {
        if (!accessories.contains(accessory)) {
            accessories.add(accessory)

            accessory.setCompany(this)
        }
    }

    fun getAccessories(): Set<Accessory> {
        return accessories
    }

    fun removeAccessory(accessory: Accessory) {
        accessories.remove(accessory)
    }

    fun addDiscount(discount: Discount) {
        if (!discounts.contains(discount)) {
            discounts.add(discount)

            discount.setCompany(this)
        }
    }

    fun getDiscounts(): Set<Discount> {
        return discounts
    }

    fun removeDiscount(discount: Discount) {
        discounts.remove(discount)
    }
}