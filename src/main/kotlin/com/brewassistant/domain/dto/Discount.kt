package com.brewassistant.domain.dto

import com.brewassistant.domain.person.PremiumPerson
import com.brewassistant.orm.AbstractJpaPersistable
import java.io.Serializable
import java.time.LocalDate
import javax.persistence.Entity
import javax.persistence.ManyToMany

@Entity
class Discount(private val value: Int,
               private val code: String,
               private val validSince: LocalDate,
               private val validTo: LocalDate) : Serializable, AbstractJpaPersistable<Discount>() {

    @ManyToMany
    private var people = mutableSetOf<PremiumPerson>()

    private var company: Company? = null

    fun addPerson(premiumPerson: PremiumPerson) {
        people.add(premiumPerson)
        premiumPerson.addDiscount(this)
    }

    fun removePerson(premiumPerson: PremiumPerson) {
        people.remove(premiumPerson)
    }

    fun setCompany(newCompany: Company) {
        if (company == newCompany) {
            return
        }
        if (company != null) {
            company!!.removeDiscount(this)
        }
        company = newCompany
        newCompany.addDiscount(this)
    }

    fun getCompany(): Company? {
        return company
    }
}